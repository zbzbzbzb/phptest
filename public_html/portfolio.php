<!DOCTYPE html>
<html lang="en-gb">

    <head>
        <meta charset="UTF-8">

        <!-- meta tags for Internet Explorer and mobile devices -->
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- meta tags data for good SEO -->
        <title>Zahir Bobat - WHO??</title>
        <meta name="description" content="A bit about an aspiring software
            developer called Zahir Bobat rising out of the UK">
        <meta name="theme-color" content="#f38a7e">

        <!-- your stylesheet and any other styling code, e.g. google fonts -->
        <link rel="stylesheet" href="css/layout.css">
        <link
            href="https://fonts.googleapis.com/css?family=Oswald%7cStaatliches"
            rel="stylesheet">


    </head>

    <body class="parallax">

        <div class="subbody">
            <header class="page-header">
                <div class="header-content par-text">
                    <h1>Who is Zahir Bobat</h1>
                    <p>A very brief and boring overview of my life</p>
                    <div class="socialmedia">
                        <a href="https://twitter.com/zfbobat" aria-label="Link to my Twitter page"><div id="twitter"></div></a>
                        <a href="https://instagram.com/zfb234" aria-label="Link to my instagram page"><div id="insta"></div></a>
                        <a
                            href="https://www.youtube.com/channel/UCJuhAjBBVEygfMuP0PZoiCg" aria-label="Link to my youtube channel"><div
                                id="youtube"></div></a>
                        <a href="https://github.com/zbzbzbzb" aria-label="Link to my github page"><div id="github"></div></a>
                    </div>
                    <img class="scroll" src="images/icons/scroll.png" alt="A
                        scroll icon to indicate to the user to scroll down the
                        page">
                </div>
            </header>
            <div class="parent">
                <nav class="page-nav">
                    <ul>
                        <li><a href="#about" aria-label="Link to part of the page"><p>About Me</p></a></li>
                        <li><a href="#build" aria-label="Link to part of the page"><p>Building PC's</p></a></li>
                        <li><a href="#images" aria-label="Link to part of the page"><p>Image Gallery</p></a></li>
                        <li><a href="#contact" aria-label="Link to part of the page"><p>Contact Me</p></a></li>
                    </ul>
                </nav>
            </div>
            <main>
                <section id="about" class="about-me container">
                    <div class="container-content">
                        <h2>About Me</h2>
                        <!-- add your 150 word maximum About Me content here -->
                        <p>
                            A highly motivated and hardworking individual, who
                            is working within the computing industry to build
                            upon a keen technological interest with the
                            intention to expand my career as a software
                            developer. Technologically
                            minded, with a methodical approach to working and an
                            eagerness to learn and develop personal skills in a
                            practical setting, I have been able
                            discover an interest in web development and Android
                            development while at work. I have worked with
                            languages such as HTML, CSS, JavaScript, PHP, SQL
                            and Java.
                        </p>

                    </div>
                </section>

                <section id="build" class="build-experience container">
                    <div class="container-content">

                        <h2>Building PC's</h2>
                        <!-- add your 200 word maximum work experience content here -->
                        <p>
                            During this time I have also been restoring my
                            sister’s old computer into a gaming PC. Since the
                            computer is very old I have had some trouble
                            especially since I was doing it on a budget. I was
                            able to fully disassemble and upgrade the PC and
                            successfuly boot to Windows 10. Since the
                            hardware I currently had was outdated and
                            insufficient for current day tasks, I decided to
                            build a custom PC within that case. This allowed me
                            to keep costs to a minimum while trying to maximise
                            performance. At the moment the airflow in
                            the case is not as efficient as I want it to be, so
                            I am hoping to buy some case fans or a new case in
                            order to equalise the
                            airflow within the case ensuring that none of the
                            components overheat, causing them to thermal
                            throttle and hinder performance. Recently I also
                            built a
                            computer for a client that was on a tight budget,
                            his budget restricted him from going to a big
                            brand shop to buuy a computer. Thus I
                            built him a computer at an extremely low budget that
                            turned out to be quite challenging due to the amount
                            of components that I needed at such a low price.
                        </p>

                    </div>
                </section>

                <section id="images" class="image-gallery container">
                    <h2>Image Gallery</h2>
                    <div class="container-content">
                        <div class="image-container">
                            <div class="an-image">
                                <!-- add image two here with 150 word maximum caption -->
                                <img src="images/car.JPG" alt="and image showing
                                    a CPU socketed in a motherboard">
                                <div class="image-caption"><p>My new MK6 Golf GTI with Diomand Cut Monza alloys</p></div>
                            </div>
                            <div class="an-image">
                                <!-- add image one here with 150 word maximum caption -->
                                <img src="images/car2.jpg" alt="an image showing
                                    my computer">
                                <div class="image-caption"><p>Another picture of my car but with my garden in the background</p></div>
                            </div>
                            <div class="an-image">
                                <!-- add image three here with 150 word maximum caption -->
                                <img src="images/tv.jpg" alt="a picture of
                                    a multi-monitor gaming setup">
                                <div class="image-caption"><p>My new LG BX OLED that is perfect for gaming and content consumption</p></div>
                            </div>

                        </div>
                    </div>
                    <p class="image-info">Tap a picture to zoom in</p>
                </section>

                <section id="contact" class="contact-me container">
                    <div class="container-content">
                        <h2>Contact Me</h2> <div class="js_availability"
                            id="js_availability"><p>Checking For availability</p></div>
                        <form action="https://formspree.io/zfbobat@gmail.com"
                            method="post">
                            <div class="stylish-input">
                                <p>
                                    <label for="contact-name">Your Name</label>
                                    <input id="contact-name" type="text"
                                        name="contact-name" />
                                </p>
                                <p>
                                    <label for="contact-email">Your email
                                        address</label>
                                    <input id="contact-email" type="email"
                                        name="contact-email" /> </p>
                                <p>
                                    <label for="contact-phone">Your phone number</label>
                                    <input id="contact-phone" type="tel"
                                        name="contact-phone" required
                                        pattern="[0-9 ()+]*" /> </p>
                                <p>
                                    <label for="contact-message">Your message</label>
                                    <textarea id="contact-message"
                                        name="contact-message" rows="5">
                                    </textarea>
                                </p>
                            </div>
                            <p id="buttons">
                                <input id="submit" type="submit" value="send my
                                    message" class="visibleSubmit"
                                    style="display:
                                    block" />
                            </p>
                        </form>
                    </div>
                </section>
            </main>
            <footer class="page-footer">
                <div class="footer-content">
                    <div class="footer-flex">
                        <p>
                            <span class="footer-text">&copy;
                                <a href="http://www.mmu.ac.uk/" title="MMU
                                    homepage">MMU 2019</a>
                            </span>

                            <span class="footer-text">
                                <a href="mailto:r.eskins@mmu.ac.uk">Richard
                                    Eskins</a>
                            </span>
                        </p>

                        <p>
                            <span class="footer-text">
                                <a
                                    href="http://validator.w3.org/check?uri=referer"
                                    title="W3C HTML Validation Service">Valid
                                    HTML</a>
                            </span>

                            <span class="footer-text">
                                <a href="https://jigsaw.w3.org/css-validator/"
                                    title="W3C CSS Validation Service">Valid CSS</a>
                            </span>
                        </p>
                        <a href="http://cooltext.com" target="_top"><img
                                src="https://cooltext.com/images/ct_pixel.gif"
                                width="80" height="15" alt="Cool Text: Logo and
                                Graphics Generator" /></a>
                    </div>

                </div>
            </footer>
        </div>
        <!--   SCIPTS    -->
        <script src="js/availability.js"></script>
        <script src="js/submitReplace.js"></script>
        <script src="js/formchecker.js"></script>

    </body>

</html>
